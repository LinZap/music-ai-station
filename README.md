# Music AI Station

使用智能運算從一首歌延伸找出你可能喜歡的一系列歌曲

## Install

```
cd iq-react2
npm install
```

## Dev Mode

```
npm run start
```

## Prod Mode

```
npm run build
```

## ENV Version

* **Node.js: v9.7.1**
* **npm: 5.6.0**

## Package Version

* **react: 16**
* **redux: 4** 
* **react-router-dom: 4** 
* **connected-react-router: 6** (棄用react-router-redux)
* **postcss-loader: 3**
* **react-intl: 4**
* **@fortawesome/react-fontawesome: 0.19**

## Dev Package Version

* **babel: 7**
* **webpack: 4**

## LICENSE

MIT - ZapLin